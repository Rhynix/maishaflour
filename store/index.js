export const state = () => ({
    articales: [
        {
            'id':1,
            'class': 'mkd-pl-item mkd-pl-masonry-large-width post-410 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-bistro portfolio-tag-cooking portfolio-tag-courses portfolio-tag-diet portfolio-tag-health',
            'style':'position: absolute; left: 0%; top: 0px;',
            'width':'900',
            'height':'426',
            'image':'/img/07/h1-gallery-img-2.jpg',
            'title':'Mediterranean Snacks',
            'desc':'Bistro'
        },
        {
            'id':2,
            'class': 'mkd-pl-item mkd-pl-masonry-default post-414 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-bistro portfolio-tag-diet portfolio-tag-food portfolio-tag-meals portfolio-tag-recipes',
            'style':'position: absolute; left: 50%; top: 0px;',
            'width':'900',
            'height':'900',
            'image':'/img/07/p-pinterest-image2.jpg',
            'title':'Healthy Breakfast',
            'desc':'Bistro'
        },
        {
            'id':3,
            'class': 'mkd-pl-item mkd-pl-masonry-default post-415 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-bistro portfolio-tag-cooking portfolio-tag-courses portfolio-tag-food portfolio-tag-meals',
            'style':'position: absolute; left: 74.9624%; top: 0px;',
            'width':'900',
            'height':'900',
            'image':'/img/07/p-pinterest-image3.jpg',
            'title':'Healtly Breakfast',
            'desc':'Bistro'
        },
        {
            'id':4,
            'class': 'mkd-pl-item mkd-pl-masonry-default post-428 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-bistro portfolio-tag-courses portfolio-tag-diet portfolio-tag-meals portfolio-tag-recipes',
            'style':'position: absolute; left: 0%; top: 330px;',
            'width':'900',
            'height':'900',
            'image':'/img/07/p-pinterest-image4.jpg',
            'title':'Healtly Breakfast',
            'desc':'Bistro'
        },
        {
            'id':5,
            'class': 'mkd-pl-item mkd-pl-masonry-default post-432 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-bistro portfolio-tag-asian portfolio-tag-cooking portfolio-tag-food portfolio-tag-health',
            'style':'position: absolute; left: 24.9624%; top: 330px;',
            'width':'900',
            'height':'900',
            'image':'/img/07/p-pinterest-image5.jpg',
            'title':'Healtly Breakfast',
            'desc':'Bistro'
        },
        {
            'id':6,
            'class': 'mkd-pl-item mkd-pl-masonry-large-width post-433 portfolio-item type-portfolio-item status-publish has-post-thumbnail hentry portfolio-category-bistro portfolio-tag-cooking portfolio-tag-food portfolio-tag-health portfolio-tag-recipes',
            'style':'position: absolute; left: 50%; top: 332px;',
            'width':'900',
            'height':'426',
            'image':'/img/07/h1-gallery-img-1.jpg',
            'title':'Healtly Breakfast',
            'desc':'Bistro'
        },
    ],
  })
  
  export const getters = {
    articales (state) {
      return state.articales
    },
  }
  
  export const mutations = {
    SET_ARTICALES (state, articales) {
      state.articales = articales
    },
  }
  
  export const actions = {
    async sercurrentlocation({ commit}, data ) {
        commit('SET_CURRENT_USER', data)
    },
  }
  